﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class RectResizer
{
    public static void ResizeRectTo(this RectTransform scalingRect, RectTransform sizeToRect)
    {
        var mainCanvas = sizeToRect.GetComponentInParent<CanvasScaler>().GetComponent<RectTransform>();
        Canvas.ForceUpdateCanvases();
        scalingRect.anchoredPosition = Vector3.zero;
        scalingRect.sizeDelta = mainCanvas.sizeDelta;
        scalingRect.pivot = sizeToRect.pivot;
        scalingRect.localScale = sizeToRect.lossyScale / mainCanvas.lossyScale.x;
        Vector2 v2 = new Vector2(sizeToRect.rect.width, sizeToRect.rect.height);
        //you also can add some lerp for animation;
        scalingRect.sizeDelta = v2;
        Vector3 pos = sizeToRect.position;
        //you also can add some lerp for animation;
        scalingRect.position = pos;
    }
}

