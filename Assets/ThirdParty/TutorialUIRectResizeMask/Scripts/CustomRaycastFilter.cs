﻿using UnityEngine;

[RequireComponent(typeof(UnityEngine.UI.GraphicRaycaster))]
public class CustomRaycastFilter : MonoBehaviour, ICanvasRaycastFilter
{
    [SerializeField]
    RectTransform _allowRaycastRect;

    public bool IsRaycastLocationValid(Vector2 clickPoint, Camera eventCamera)
    {
        if (_allowRaycastRect != null)
        {
            Vector2 childPos = _allowRaycastRect.worldToLocalMatrix.MultiplyPoint(clickPoint);
            if (_allowRaycastRect.rect.Contains(childPos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else {
            Debug.LogWarning("_allowRaycastRect does not set!");
            return true;
        }
    }
}