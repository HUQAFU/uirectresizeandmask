﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour {
    [SerializeField]
    RectTransform _scalingTransform;

    [SerializeField]
    RectTransform _parentCanvas;

    float _timeToNectRandomRect = 2;
    float _currTime = 0;
    RectTransform[] _rects;

    // Use this for initialization
    void Start () {
        _rects = _parentCanvas.GetComponentsInChildren<RectTransform>();
    }

    private void Update()
    {
        if (_rects != null && _rects.Length > 0)
        {
            if (_currTime < _timeToNectRandomRect)
            {
                _currTime += Time.deltaTime;
            }
            else
            {
                //Resizes rect (for masking in tutorial, and etc - use Masking UI shaders and CustomRaycastFilter - see DemoScene)
                _scalingTransform.ResizeRectTo(_rects[Random.Range(0, _rects.Length)]);
                _currTime = 0;
            }
        }
    }

}
